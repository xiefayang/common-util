import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.io.*;
import java.util.Map;
import java.util.Set;

/**
 * Created by wangnian on 2017/4/18.
 */
public class Test2 {

    public static void main(String[] args) throws IOException {
        File file = new File("D:/json.txt");
        InputStream inputStream = new FileInputStream(file);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        StringBuilder stringBuilder = new StringBuilder();
        String lineTxt;
        while ((lineTxt = bufferedReader.readLine()) != null) {
            stringBuilder.append(lineTxt);
        }
        JSONObject jsonObject = JSON.parseObject(stringBuilder.toString());
        System.out.println(jsonObject.toJSONString());
        for (Map.Entry<String, Object> m : jsonObject.entrySet()) {
            System.out.println(m.getKey());
            System.out.println(m.getValue());
        }

    }
}
