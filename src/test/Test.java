import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Test implements Runnable {
    static ExecutorService executorService1 = Executors.newSingleThreadExecutor();
    private String name;

    public Test(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        executorService1.execute(new Test("1"));
        executorService1.execute(new Test("2"));
        executorService1.execute(new Test("3"));
    }

    public void run() {
        int i = 0;

        while (i < 5) {
            System.out.println("线程" + this.name + "开始");
            ++i;
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException var3) {
                var3.printStackTrace();
            }
        }

    }


}
