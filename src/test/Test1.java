import com.common.util.HttpUtil;
import jodd.http.HttpRequest;
import jodd.http.net.SSLSocketHttpConnectionProvider;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.zookeeper.*;
import org.junit.Test;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by wangnian on 2016/12/8.
 */
public class Test1 {

    @Test
    public void test() throws Exception {
        HttpRequest httpRequest = HttpRequest.post("http://www.baidu.com");
        Map map = new HashMap();
        map.put("abc", "abc");
        httpRequest.query(map);

        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        FileInputStream instream = new FileInputStream(new File(""));
        try {
            keyStore.load(instream, "1370249302".toCharArray());
        } finally {
            instream.close();
        }
        SSLContext sslcontext = SSLContexts.custom()
                .loadKeyMaterial(keyStore, "1370249302".toCharArray())
                .build();


        httpRequest.body("sjfkdjfkdjkf");
        System.out.println(httpRequest.send());
    }


    @Test
    public void test2() throws Exception {
        String password = "";
        SSLContext ctx = SSLContext.getInstance("SSL");

        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");

        KeyStore ks = KeyStore.getInstance("JKS");

        ks.load(new FileInputStream("src/ssl/kserver.keystore"), password.toCharArray());

        kmf.init(ks, password.toCharArray());

        ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

        SSLSocketHttpConnectionProvider sslSocketHttpConnectionProvider = new SSLSocketHttpConnectionProvider(ctx);

        HttpRequest httpRequest = new HttpRequest();

        httpRequest.withConnectionProvider(sslSocketHttpConnectionProvider);

        httpRequest.connection();

    }


    @Test
    public void test3() {
        long a = System.currentTimeMillis();
        String string = HttpUtil.get("http://localhost:9002/");
        long a1 = System.currentTimeMillis();
        System.out.println(a1 - a);
    }

    @Test
    public void test4() {
        long a = System.currentTimeMillis();
        String string = HttpRequest.get("http://localhost:9002/").send().body();
        long a1 = System.currentTimeMillis();
        System.out.println(a1 - a);
    }

    private static final int TIME_OUT = 3000;
    private static final String HOST = "localhost:2181";

    @Test
    public void test5() throws Exception {

        ZooKeeper zookeeper = new ZooKeeper(HOST, TIME_OUT, new Watcher() {
            @Override
            public void process(WatchedEvent watchedEvent) {
                System.out.println("触发了" + watchedEvent.getType() + "事件");
            }
        });
        System.out.println("=========创建节点===========");
        if (zookeeper.exists("/test", true) == null) {
            zookeeper.create("/test", "哈哈哈哈哈".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
        }
        System.out.println("=============查看节点数据===============");
        System.out.println(new String(zookeeper.getData("/test", true, null)));

        System.out.println("=========修改节点的数据==========");
        String data = "修改之后的数据";
        zookeeper.setData("/test", data.getBytes(), -1);

        System.out.println("========查看修改的节点是否成功=========");
        System.out.println(new String(zookeeper.getData("/test", true, null)));

        System.out.println("=======删除节点==========");
        zookeeper.delete("/test", -1);

        System.out.println("==========查看节点是否被删除============");
        System.out.println("节点状态：" + zookeeper.exists("/test", true));

        zookeeper.close();
    }

    public static void main(String[] args) throws InterruptedException {
        Object object=new Object();
        synchronized (object){

            if (object!=null){

            }
            object.wait();
            object.notify();
        }
    }
}

