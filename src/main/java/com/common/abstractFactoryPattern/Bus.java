package com.common.abstractFactoryPattern;

/**
 * Created by Administrator on 2017/3/6.
 */
public interface Bus {
    void produce();
}

class BmBus implements Bus {

    @Override
    public void produce() {
        System.out.println("生产宝马公交车");
    }
}

class BcBus implements Bus {

    @Override
    public void produce() {
        System.out.println("生产奔驰公交车");
    }
}