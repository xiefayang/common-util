package com.common.abstractFactoryPattern;

/**
 * 大的工厂
 * Created by wangnian on 2017/3/3.
 */
public abstract class AabstractFactory {

    public static AabstractFactory createFactory(String type) {
        switch (type) {
            case "BM":
                return new BmFactory();
            case "BC":
                return new BcFactory();
        }
        return null;
    }

    public abstract Bus createBus();

    public abstract Car createCar();

}

class BmFactory extends AabstractFactory {

    @Override
    public Bus createBus() {
        return new BmBus();
    }

    @Override
    public Car createCar() {
        return new BmCar();
    }
}

class BcFactory extends AabstractFactory {

    @Override
    public Bus createBus() {
        return new BcBus();
    }

    @Override
    public Car createCar() {
        return new BcCar();
    }
}


