package com.common.abstractFactoryPattern;

/**
 * Created by Administrator on 2017/3/6.
 */
public class Main {

    public static void main(String[] args) {
        AabstractFactory aabstractFactory = AabstractFactory.createFactory("BM");
        aabstractFactory.createBus().produce();

        AabstractFactory aabstractFactory1 = AabstractFactory.createFactory("BC");
        aabstractFactory1.createCar().produce();

    }
}
