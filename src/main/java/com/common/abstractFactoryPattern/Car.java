package com.common.abstractFactoryPattern;

/**
 * Created by Administrator on 2017/3/6.
 */
public interface Car {
    void produce();
}


class BmCar implements Car {

    @Override
    public void produce() {
        System.out.println("生产宝马小汽车");
    }
}

class BcCar implements Car {

    @Override
    public void produce() {
        System.out.println("生产奔驰小汽车");
    }
}


