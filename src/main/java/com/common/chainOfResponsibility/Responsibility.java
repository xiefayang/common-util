package com.common.chainOfResponsibility;

import org.junit.Test;


/**
 * Created by Administrator on 2017/3/3.
 */
public abstract class Responsibility {

    public Responsibility responsibility;

    public Responsibility getResponsibility() {
        return responsibility;
    }

    public void setResponsibility(Responsibility responsibility) {
        this.responsibility = responsibility;
    }

    public abstract void execute(String str);

}
