package com.common.chainOfResponsibility;

/**
 * Created by Administrator on 2017/3/3.
 */
public class Execute2 extends Responsibility {

    @Override
    public void execute(String str) {
        if ("B".equals(str)) {
            System.out.println("Execute2我已经处理了");
        } else {
            if (getResponsibility() != null) {
                getResponsibility().execute(str);
            }

        }
    }
}
