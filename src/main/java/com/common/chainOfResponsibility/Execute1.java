package com.common.chainOfResponsibility;

/**
 * Created by Administrator on 2017/3/3.
 */
public class Execute1 extends Responsibility {

    @Override
    public void execute(String str) {
        if ("A".equals(str)) {
            System.out.println("Execute1我已经处理了");
        } else {
            if (getResponsibility() != null) {
                getResponsibility().execute(str);
            }

        }
    }
}
