package com.common.chainOfResponsibility;

/**
 * Created by Administrator on 2017/3/10.
 */
public class MainTest {

    public static void main(String[] args) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        MainTest mainTest = new MainTest();
        mainTest.aa();

    }

    public void aa() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        int[] a = {1, 2, 3};
        for (int i = 0; i < a.length - 1; i++) {
            getResponsibility(a[i]).setResponsibility(getResponsibility(a[i + 1]));
        }
        getResponsibility(a[0]).execute("C");
    }

    Execute1 execute1 = new Execute1();
    Execute2 execute2 = new Execute2();
    Execute3 execute3 = new Execute3();


    public Responsibility getResponsibility(int i) {
        switch (i) {
            case 1:
                return execute1;
            case 2:
                return execute2;
            case 3:
                return execute3;
            default:
                break;
        }
        return null;
    }
}
