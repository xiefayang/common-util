package com.common.strategyPattern;

/**
 * Created by wangnian on 2017/4/25.
 */
public class Bowl {
    private Ifood ifood;

    public Bowl(Ifood ifood) {
        this.ifood = ifood;
    }

    public void eat() {
        ifood.eat();
    }
}
