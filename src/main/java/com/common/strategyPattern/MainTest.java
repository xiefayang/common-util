package com.common.strategyPattern;

/**
 * Created by wangnian on 2017/4/25.
 */
public class MainTest {

    public static void main(String[] args) {
        Bowl bowl=new Bowl(new FrenchBean());
        bowl.eat();
    }
}
