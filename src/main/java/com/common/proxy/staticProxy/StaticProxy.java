package com.common.proxy.staticProxy;

/**
 * Created by Administrator on 2016/12/28.
 */
public class StaticProxy implements  UserService {

    UserService userService;
    public StaticProxy(UserServiceImpl userService){

            this.userService=userService;
    }

    @Override
    public void add() {
        userService.add();
        System.out.println("这是代理类做的事");
    }

    @Override
    public void delete() {
        userService.delete();
    }
}
