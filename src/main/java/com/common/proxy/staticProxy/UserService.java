package com.common.proxy.staticProxy;

/**
 * 接口
 */
public interface UserService {

    /**
     * 添加方法
     */
    void add();

    /**
     * 删除方法
     */
    void delete();

}  