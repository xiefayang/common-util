package com.common.proxy.staticProxy;


/**
 * 接口实现类
 */
public class UserServiceImpl implements UserService {


    @Override
    public void add() {
        System.out.println("--------------------add---------------");
    }

    @Override
    public void delete() {
        System.out.println("--------------------delete---------------");
    }
}  