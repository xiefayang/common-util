package com.common.proxy.cglibProxy;

import net.sf.cglib.proxy.Enhancer;

/**
 * 工厂类，生成增强过的目标类（已加入切入逻辑）
 *
 * @author typ
 */
public class Factory {
    /**
     * 获得增强之后的目标类，即添加了切入逻辑advice之后的目标类
     *
     * @return
     */
    public static Object getInstance(Object object) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(object.getClass());
        //回调方法的参数为代理类对象CglibProxy，最后增强目标类调用的是代理类对象CglibProxy中的intercept方法  
        enhancer.setCallback(new CglibProxy());
        // 此刻，userService，而是增强过的目标类
        return enhancer.create();
    }
}