package com.common.proxy.cglibProxy;

/**
 * @author typ
 */
public class Test {

    public static void main(String[] args) {
        // userService
        UserServiceImpl userService = (UserServiceImpl) Factory.getInstance(new UserServiceImpl());
        userService.add();
    }
}  