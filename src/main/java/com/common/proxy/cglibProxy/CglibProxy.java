package com.common.proxy.cglibProxy;

import java.lang.reflect.Method;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 此为代理类，用于在pointcut处添加advise
 *
 * @author typ
 */
public class CglibProxy implements MethodInterceptor {

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("这是开始");
        methodProxy.invokeSuper(o, objects);
        System.out.println("这是结束");
        return null;
    }
}