package com.common.util;

import jodd.datetime.JDateTime;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 创建日期 2016/12/9
 * 时间工具
 */
public class DateUtil {
    /**
     * 格式 2015-08-07
     */
    public static final String DateFormat_yyyyMMdd = "yyyy-MM-dd";
    /**
     * 格式 15:25:30
     */
    public static final String DateFormat_HHmmss = "HH:mm:ss";

    /**
     * 格式 2015-08
     */
    public static final String DateFormat_yyyyMM = "yyyy-MM";

    /**
     * 格式 2015-08-07 11:44:18
     */
    public static final String DateFormat_yyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss";

    /**
     * 格式 2015-08-07 11:44:18:591
     */
    public static final String DateFormat_yyyyMMddHHmmssSSS = "yyyy-MM-dd HH:mm:ss.SSS";

    /**
     * 格式 150807114418
     */
    public static final String DateFormat_yyMMddHHmmss = "yyMMddHHmmss";

    /**
     * 格式 20140416142030
     */
    public static final String DateFormat_TimeStemp_yyyyMMddHHmmss = "yyyyMMddHHmmss";

    /**
     * 格式 20150807114418591
     */
    public static final String DateFormat_TimeStemp = "yyyyMMddHHmmssSSS";

    /**
     * 格式 2015-08-13T16:49:02.039+0800
     */
    public static final String DateFormat_yyyyMMddTHHmmssSSSZ = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    /**
     * 格式 2015-11-09T00:00:08.000+08:00
     */
    public static final String DateFormat_yyyyMMddTHHmmssSSSXXX = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";


    /**
     * 日期转化为字符串
     *
     * @param format
     * @return
     */
    public static String parseToString(Date date, String format) {
        DateFormat df = new SimpleDateFormat(format);
        String strDate = df.format(date);
        return strDate;
    }

    /**
     * 字符串转化为日期
     *
     * @param str
     * @param format
     * @return
     */
    public static Date parseToDate(String str, String format) {
        DateFormat df = new SimpleDateFormat(format);
        Date date = null;
        try {
            date = df.parse(str);
        } catch (ParseException e) {

        }
        return date;
    }


    /**
     * 格式化日期
     */
    public static Date getFormatDate(Date date, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            String strDate = sdf.format(date);
            Date newDate = sdf.parse(strDate);
            return newDate;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 日期加年
     *
     * @param date
     * @param years
     * @return
     */
    public static Date addYear(Date date, int years) {
        JDateTime JDateTime = new JDateTime(date);
        JDateTime.addYear(years);
        return JDateTime.convertToDate();
    }

    /**
     * 日期减年
     *
     * @param date
     * @param years
     * @return
     */
    public static Date subYear(Date date, int years) {
        JDateTime JDateTime = new JDateTime(date);
        JDateTime.subYear(years);
        return JDateTime.convertToDate();
    }


    /**
     * 日期加月数
     *
     * @param date
     * @return
     */
    public static Date addMonth(Date date, int months) {
        JDateTime JDateTime = new JDateTime(date);
        JDateTime.addMonth(months);
        return JDateTime.convertToDate();
    }

    /**
     * 日期减月数
     *
     * @param date
     * @return
     */
    public static Date subMonth(Date date, int months) {
        JDateTime JDateTime = new JDateTime(date);
        JDateTime.subMonth(months);
        return JDateTime.convertToDate();
    }


    /**
     * 日期加天数
     *
     * @param date
     * @param days
     * @return
     */
    public static Date addDay(Date date, int days) {
        JDateTime JDateTime = new JDateTime(date);
        JDateTime.addDay(days);
        return JDateTime.convertToDate();
    }

    /**
     * 日期减天数
     *
     * @param date
     * @param days
     * @return
     */
    public static Date subDay(Date date, int days) {
        JDateTime JDateTime = new JDateTime(date);
        JDateTime.subDay(days);
        return JDateTime.convertToDate();
    }


    /**
     * 日期加小时数
     *
     * @param date
     * @return
     */
    public static Date addHour(Date date, int hours) {
        JDateTime JDateTime = new JDateTime(date);
        JDateTime.addHour(hours);
        return JDateTime.convertToDate();
    }


    /**
     * 日期减小时数
     *
     * @param date
     * @return
     */
    public static Date subHour(Date date, int hours) {
        JDateTime JDateTime = new JDateTime(date);
        JDateTime.subHour(hours);
        return JDateTime.convertToDate();
    }

    /**
     * 日期加分钟数
     *
     * @param date
     * @return
     */
    public static Date addMinutes(Date date, Integer minutes) {
        JDateTime JDateTime = new JDateTime(date);
        JDateTime.addMinute(minutes);
        return JDateTime.convertToDate();
    }

    /**
     * 日期加分钟数
     *
     * @param date
     * @return
     */
    public static Date subMinute(Date date, Integer minutes) {
        JDateTime JDateTime = new JDateTime(date);
        JDateTime.subMinute(minutes);
        return JDateTime.convertToDate();
    }

    /**
     * 日期加秒
     *
     * @param date
     * @return
     */
    public static Date addSecond(Date date, int second) {
        JDateTime JDateTime = new JDateTime(date);
        JDateTime.addSecond(second);
        return JDateTime.convertToDate();
    }

    /**
     * 日期加秒
     *
     * @param date
     * @return
     */
    public static Date subSecond(Date date, int second) {
        JDateTime JDateTime = new JDateTime(date);
        JDateTime.subSecond(second);
        return JDateTime.convertToDate();
    }

    /**
     * 获取date2-date1的年份差
     *
     * @param date1
     * @param date2
     * @return
     */
    public static int getDateBetweenYear(Date date1, Date date2) {
        Calendar c1 = Calendar.getInstance();
        c1.setTime(date1);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(date2);
        int yearSub = c2.get(Calendar.YEAR) - c1.get(Calendar.YEAR);
        return yearSub;
    }

    /**
     * 获取date2-date1的月份差
     *
     * @param date1
     * @param date2
     * @return
     */
    public static int getDateBetweenMonth(Date date1, Date date2) {
        Calendar c1 = Calendar.getInstance();
        c1.setTime(date1);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(date2);
        int yearSub = c2.get(Calendar.YEAR) - c1.get(Calendar.YEAR);
        int month1 = c1.get(Calendar.MONTH);
        int month2 = c2.get(Calendar.MONTH);
        return yearSub * 12 - (month1 - month2);
    }

    /**
     * 计算两个时间的天数之差
     *
     * @param date1
     * @param date2
     * @return
     */
    public static int daysBetweenTwoDate(Date date1, Date date2) {
        Calendar c1 = Calendar.getInstance();
        c1.setTime(date1);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(date2);
        int daySub = c2.get(Calendar.DAY_OF_YEAR) - c1.get(Calendar.DAY_OF_YEAR);
        return daySub;
    }

    /**
     * 计算两个时间的分钟之差
     *
     * @param date1
     * @param date2
     * @return
     */
    public static int minutesBetweenTwoDate(Date date1, Date date2) {
        long longValue1 = date1.getTime();
        long longValue2 = date2.getTime();
        int minutes = (int) ((longValue2 - longValue1) / (1000 * 60));
        return minutes;
    }


    /**
     * 计算两个时间的秒之差
     *
     * @param date1
     * @param date2
     * @return
     */
    public static int secondBetweenTwoDate(Date date1, Date date2) {
        long longValue1 = date1.getTime();
        long longValue2 = date2.getTime();
        int second = (int) ((longValue2 - longValue1) / (1000));
        return second;
    }


    /**
     * 获取该日期的所在月的第一天，零时零分零秒
     *
     * @param date
     * @return
     * @throws ParseException
     */
    public static Date getDateThisFirstDay(Date date) {
        date = getFormatDate(date, DateFormat_yyyyMMdd);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTime();
    }

    /**
     * 获取该日期的所在月的最后一天，零时零分零秒
     *
     * @param date
     * @return
     * @throws ParseException
     */
    public static Date getDateThisLastDay(Date date) {
        date = getFormatDate(date, DateFormat_yyyyMMdd);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return calendar.getTime();
    }

    /**
     * 获取时间范围内，每一天的零时零分零秒的日期列表（包括起止日期）
     *
     * @param startDate
     * @param endDate
     * @return
     * @throws ParseException
     */
    public static List<Date> getDateListByEveryDay(Date startDate, Date endDate) {
        startDate = getFormatDate(startDate, DateFormat_yyyyMMdd);
        endDate = getFormatDate(endDate, DateFormat_yyyyMMdd);
        List<Date> result = new ArrayList<Date>();
        for (Date date = startDate; date.compareTo(endDate) < 1; date = addDay(date, 1)) {
            result.add(date);
        }
        return result;
    }

    /**
     * 获取日期的月份
     *
     * @param date
     * @return
     */
    public static int getMonthOfDate(Date date) {
        JDateTime jt = new JDateTime(date);
        return jt.getMonth();
    }

    /**
     * 获取日期的日期
     *
     * @param date
     * @return
     */
    public static int getDayOfMonth(Date date) {
        JDateTime jt = new JDateTime(date);
        return jt.getDayOfMonth();
    }

    /**
     * 通过哪一年的第几周得到日期区间
     *
     * @param week
     * @param year
     * @return
     */
    public static String getWeekDate(int week, int year) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        StringBuilder sb = new StringBuilder();
        cal.clear();
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.WEEK_OF_YEAR, week);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        System.out.println(sdf.format(cal.getTime()));
        sb.append(sdf.format(cal.getTime()));
        cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        System.out.println(sdf.format(cal.getTime()));
        sb.append("至");
        sb.append(sdf.format(cal.getTime()));
        return sb.toString();
    }

}
