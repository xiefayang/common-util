package com.common.util;

import jodd.http.HttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * 创建日期 2016/12/9
 * HTTP请求工具
 */
public class HttpUtil {

    private static Logger logger = LoggerFactory.getLogger(HttpUtil.class);

    /**
     * get请求
     *
     * @param url
     * @return
     */
    public static String get(String url) {
        logger.info("GET请求的地址 url:{}", url);
        return HttpRequest.get(url).send().bodyText();
    }

    /**
     * 带参数的get请求
     *
     * @param url
     * @param parMap
     * @return
     */
    public static String get(String url, Map<String, String> parMap) {
        logger.info("GET请求的地址 url{},parMap:{}", url, parMap);
        HttpRequest httpRequest = HttpRequest.get(url);
        httpRequest.query(parMap);
        return httpRequest.send().bodyText();
    }

    /**
     * 带参数post请求
     *
     * @param url
     * @return
     */
    public static String post(String url, Map<String, String> parMap) {
        logger.info("POST请求的地址 url{},parMap:{}", url, parMap);
        HttpRequest httpRequest = HttpRequest.post(url);
        httpRequest.query(parMap);
        return httpRequest.send().bodyText();
    }


    /**
     * RAW Body Post请求
     *
     * @param url
     * @param body
     * @return
     */
    public static String post(String url, String body) {
        logger.info("BODY请求的地址 url{},body:{}", url, body);
        HttpRequest httpRequest = HttpRequest.post(url);
        httpRequest.bodyText(body);
        return httpRequest.send().bodyText();
    }


}
