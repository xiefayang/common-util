package com.common.util;

import org.quartz.*;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdSchedulerFactory;

import java.util.Date;

/**
 * Created by Administrator on 2017/3/6.
 */
public class JobUtil {

    /**
     * 添加任务
     *
     * @param job             任务类
     * @param date            任务时间
     * @param jobDetailName   任务消息名字
     * @param triggerIdentity 触发器的唯一名
     * @param description     触发器的描述
     */
    public void addJob(Job job, Date date, String jobDetailName, String triggerIdentity, String description) {
        //这是job类的任务
        JobDetailImpl jobDetail = new JobDetailImpl();
        jobDetail.setName(jobDetailName);
        jobDetail.setJobClass(job.getClass());
        //作业触发器
        Trigger trigger = TriggerBuilder.newTrigger().withIdentity(triggerIdentity).withDescription(description)
                // .withSchedule(SimpleScheduleBuilder.repeatSecondlyForever(3))//循环间隔多久
                .startAt(date)//执行时间
                .build();
        //作业调度器
        try {
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            scheduler.scheduleJob(jobDetail, trigger);
            scheduler.start();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
}
