package com.common.util;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

/**
 * 创建日期 2016/12/9
 * 校验工具
 */
public class ValidatorUtil {

    /**
     * 检查字符串是否为空
     *
     * @param strs
     * @return
     */
    public static boolean isStringsNotNull(Object... strs) {
        for (Object str : strs) {
            if (str == null || str.toString().trim().length() == 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * 检查对象的属性是否为空
     *
     * @param obj
     * @param attributeName
     * @return 属性中一个值为null时，返回false;否则返回true
     */
    public static boolean isObjectAttributeNotNull(Object obj, String... attributeName) {
        Class<? extends Object> clazz = obj.getClass();
        for (String attribute : attributeName) {
            try {
                PropertyDescriptor pd = new PropertyDescriptor(attribute, clazz);
                Method getMethod = pd.getReadMethod();//获取get方法
                Object value = getMethod.invoke(obj);//执行get方法返回一个object
                if (value == null) {//检查属性的值为空
                    return false;
                }
            } catch (Exception ex) {
                //logger.info("get object attribute value exception {}", ex);
                return false;
            }
        }
        return true;
    }
}
